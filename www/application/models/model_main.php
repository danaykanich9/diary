<?php
    class Model_Main extends Model
    {
        private static $pdo;
        function __construct()
        {
            include "application/core/connection.php";
            self::$pdo = new PDO("mysql:host=".host.";dbname=".database,user,password); 
        }
        function getLast10Records()
        {
            if(is_null($data))
            {
                $arr = self::$pdo->query("SELECT * FROM records");
                $arr = $arr->fetchAll();
                $arr = array_reverse($arr);
                $res = [];
                if(count($arr)>=10)
                    for($i=0;$i<10;$i++)
                        $res[]=$arr[$i];
                else 
                    for($i=0;$i<count($arr);$i++)
                        $res[]=$arr[$i];
                return $res;
            }
        }
    }
?>