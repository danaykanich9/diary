<?php
    class Model_User extends Model
    {
        private static $pdo;
        function __construct()
        {
            include "application/core/connection.php";
            self::$pdo = new PDO("mysql:host=".host.";dbname=".database,user,password); 
        }
        function signIn($data)//data[0] - login, data[1] - password
        {
            if(count($data)==2)
            {
                $login = $data[0];
                $password = $data[1];
                $res = self::$pdo->prepare("SELECT * FROM users 
                                            WHERE login=:login AND password=:password");
                $res->bindParam(login,$login);
                $res->bindParam(password,$password);
                $arr = $res->fetchAll();
                if(count($arr)>0)
                    return "Successful authorize";
                return "Wrong login or password";
            }
            else return "Wrong data";
        }
        function signUp($data)//data[0] - email, data[1] - login, data[2] - password, data[3] - confirmpass
        {
            if(count($data)==4)
            {
                $email = $data[0];
                $login = $data[1];
                $password = $data[2];
                $confpass = $data[3];
                if($password==$confpass)
                {
                    $r=self::$pdo->prepare("INSERT INTO users('email','login','password')
                                            VALUES (:email,:login,:password");
                    $r->bindParam(email,$email);
                    $r->bindParam(login,$login);
                    $r->bindParam(password,$password);
                    $r->execute();
                    return "Successful registry";
                }
                else return "Passwords don't match";
            }
            else return "Wrong data";
        }
    }
?>