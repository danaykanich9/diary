<?php
    class Model_Record extends Model
    {
        private static $pdo;
        function __construct()
        {
            include "application/core/connection.php";
            self::$pdo = new PDO("mysql:host=".host.";dbname=".database,user,password); 
        }
        function getUserRecords($user_name)
        {
            $res = self::$pdo->prepare("SELECT * FROM records 
                                        WHERE user=:user");
            $res->bindParam(user,$user_name);
            $res=$res->fetchAll();
            return $res;
        }
        function changeType($record_id,$type)//type - public/private
        {
            $r=self::$pdo->prepare("UPDATE records
                                    SET type=:type
                                    WHERE id=:id");
            $r->bindParam(type,$type)
            $r->bindParam(id,$record_id);
            $r->execute();
        }
        function addRecord($data)//data[0]-title,data[1]-description,$data[2]-date_time,$data[3]-type,$data[4]-user_id
        {
            if(count($data)==5)
            {
                $title=$data[0];
                $description=$data[1];
                $date_time=$data[2];
                $type=$data[3];
                $user_id=$data[4];
                $r=self::$pdo->prepare("INSERT INTO records('title','description','date','type','user_id')
                                            VALUES (:title,:descr,:dt,:type,:user_id");
                $r->bindParam(title,$title);
                $r->bindParam(descr,$description);
                $r->bindParam(dt,$date_time);
                $r->bindParam(type,$type);
                $r->bindParam(user_id,$user_id);
                $r->execute();
                return "Record was successful added";
            }
            else return "Wrong data";
        }

    }
?>