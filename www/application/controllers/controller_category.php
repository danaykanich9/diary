<?php
    class Controller_Category extends Controller
    {
        function __construct()
        {
            $this->model=new Model_Category();
            $this->view=new View();
        }
        function action_index()
        {
            $data=$this->model->getData();
            $this->view->generate("view_categories.php","view_template.php",$data);
        }
        function action_details($id)
        {
            $data=$this->model->get_by_id($id);
            if(count($data)==0)
                $this->view->generate("view_error.php","view_template.php","No categories");    
            else 
                $this->view->generate("view_categories_details.php","view_template.php",$data);
        }
        function action_create($name=null)
        {
            if($name==null)
                $this->view->generate("view_categories_create.php","view_template.php",null);
            else
                $this->model->createCategory($name);
        }

    }
?>