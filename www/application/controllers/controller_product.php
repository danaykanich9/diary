<?php
    class Controller_User extends Controller
    {
        function __construct()
        {
            $this->model=new Model_User();
            $this->view=new View();
        }
        function action_index()
        {
            $data=$this->model->getData();
            $this->view->generate("view_products.php","view_template.php",$data);
        }
        function action_details($id)
        {
            $data=$this->model->get_by_id($id);
            if(count($data)==0)
                $this->view->generate("view_error.php","view_template.php","No products");    
            else 
                $this->view->generate("view_products_details.php","view_template.php",$data);
        }
    }
?>