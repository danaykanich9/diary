<?php
    class Controller_Main extends Controller
    {
        function __construct()
        {
            $this->model=new Model_Main();
            $this->view=new View();
        }
        public function action_index()
        {
            $data=$this->model->getData();
            $this->view->generate("view_main.php","view_template.php",$data);
        }
        public function action_details($data=null)
        {
            if(is_null($data))
                $data="empty data";
            $data=$this->model->changeText($data);
            $this->view->generate("view_details.php","view_template.php",$data);
        }
    }

?>